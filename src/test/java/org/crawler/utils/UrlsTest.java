package org.crawler.utils;

import static org.crawler.utils.Urls.verifyUrl;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class UrlsTest {

    @Test
    public void verifyUrlTest() {
	assertTrue("".equals(verifyUrl(null)));

	assertTrue("http://www.google.com".equals(verifyUrl("  www.google.com#url")));
	assertTrue("".equals(verifyUrl("http")));
	assertTrue("".equals(verifyUrl("http:/")));
	assertTrue("".equals(verifyUrl("http://")));
	assertTrue("".equals(verifyUrl("http:")));
	assertTrue("".equals(verifyUrl("https:")));

	assertTrue("".equals(verifyUrl("http:qwaefwefwef")));
    }
    
}
