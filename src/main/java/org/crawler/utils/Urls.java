package org.crawler.utils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.common.base.Charsets;
import com.google.common.collect.Sets;

/**
 * Утилитный класс, который помогает валидировать ссылки. Также записывает и
 * считывает их из файла.
 * 
 * @author volyihin@gmail.com
 * 
 */
public class Urls {
    private static final String OUTPUT = "src/main/resources/output.txt";
    private static final String INPUT = "src/main/resources/input.txt";
    private static final Logger log = Logger.getLogger(Urls.class);

    /**
     * @param url
     * @return валидный url или пустую строку
     */
    public static String verifyUrl(String url) {

	URL verifiedUrl = null;

	if (url == null)
	    return "";

	if (("http".equals(url)) || ("http:/".equals(url)) || ("http://".equals(url)) || ("http:".equals(url)))
	    return "";
	if (url.toLowerCase().startsWith("https:"))
	    return "";

	if (url.contains("mailto"))
	    return "";

	url = url.trim();

	if (url.contains("#"))
	    url = url.substring(0, url.indexOf("#"));

	if (url.endsWith("/"))
	    url = url.substring(0, url.length() - 1);

	try {

	    if (url.toLowerCase().startsWith("http://"))
		return new URL(url).toString();

	    verifiedUrl = new URL("http://" + url);

	} catch (MalformedURLException e) {
	    return "";
	}

	return verifiedUrl.toString();
    }

    /**
     * Считывает список доменов из файла
     * 
     * @return
     */
    public static Set<String> getUrlsList() {

	Set<String> urls = Sets.newHashSet();
	try (Scanner scanner = new Scanner(Paths.get(INPUT))) {
	    while (scanner.hasNextLine()) {
		String url = scanner.nextLine().trim();
		urls.add(verifyUrl(url).toString());
	    }
	} catch (Exception e) {
	    log.error("Не удалось получить список доменов из файла ", e);
	}

	return urls;
    }

    /**
     * Записывает закрауленные ссылки в файл
     * 
     * @param result
     */
    public static void writeResult(Set<String> result) {

	Path outputPath = Paths.get(OUTPUT);
	try {

	    if (!Files.exists(outputPath))
		Files.createFile(outputPath);
	    Files.write(outputPath, result, Charsets.UTF_8);
	} catch (IOException e) {
	    log.error("Не удалось записать в файл", e);
	}

    }
}
