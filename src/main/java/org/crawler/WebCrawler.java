package org.crawler;

import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newLinkedHashSet;
import static org.crawler.utils.Urls.verifyUrl;
import static org.jsoup.Jsoup.connect;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.jsoup.Connection.Response;
import org.jsoup.HttpStatusException;
import org.jsoup.UnsupportedMimeTypeException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.common.base.Stopwatch;
import com.google.common.util.concurrent.RateLimiter;

import static com.google.common.base.Stopwatch.createStarted;
import static com.google.common.util.concurrent.RateLimiter.create;

/**
 * 
 * Краулер. Обходит ссылки на странице по алгоритму "обход в ширину"
 * Останавливается когда количество ссылок больше {{@link #maxLinkCount}
 * 
 * @author volyihin@gmail.com
 * 
 */
public class WebCrawler implements Callable<Map<String, Set<String>>> {
    private static final Logger log = Logger.getLogger(WebCrawler.class);
    /**
     * Длина тела стрницы в байтах
     */
    private static final int BODY_LENGTH = 512000;
    /**
     * Таймаут загрузки страницы
     */
    private int pageTimeout = 2000;
    /**
     * Количество ссылок на одной странице
     */
    private int maxLinkCount = 100;
    /**
     * Ограничитель, не пускающий чаще чем 1 секунда к одному домену
     */
    private final RateLimiter rateLimiter = create(1.0);
    /**
     * Секундомер
     */
    private Stopwatch stopwatch = createStarted();
    /**
     * список пройденных адресов
     */
    private LinkedHashSet<String> crawledUrls = newLinkedHashSet();
    /**
     * Ссылка верзнего уровня
     */
    private String firstUrl;
    /**
     * Предыдущая ссылка
     */
    private String previosUrl = "";
    /**
     * Список адресов,кот. еще нужно пройти breadth-first search
     */
    private LinkedHashSet<String> urlsToCrawl = newLinkedHashSet();

    public WebCrawler(String firstUrl, int pageTimeout, int maxLinkCount) {
	super();
	this.pageTimeout = pageTimeout;
	this.maxLinkCount = maxLinkCount;
	this.firstUrl = firstUrl;
    }

    private void crawlUrl(String url) {
	this.crawledUrls.add(url);
    }

    @Override
    public Map<String, Set<String>> call() {
	Map<String, Set<String>> callResult = newHashMap();

	firstUrl = verifyUrl(firstUrl).toString();

	if (isNullOrEmpty(firstUrl))
	    return callResult;

	urlsToCrawl.add(firstUrl);

	while (!urlsToCrawl.isEmpty() && crawledUrls.size() <= maxLinkCount) {

	    String url = getNextUrl();

	    if (isNullOrEmpty(url))
		continue;

	    // подождем секундочку если предыдущий запрос был к этому же домену
	    if (url.contains(previosUrl))
		rateLimiter.acquire();

	    log.debug(url);

	    Document doc = null;
	    Response response = null;
	    try {
		response = connect(url).timeout(pageTimeout).maxBodySize(BODY_LENGTH).execute();
		doc = response.parse();
		previosUrl = new URL(url).getHost();

	    } catch (HttpStatusException | UnsupportedMimeTypeException | SocketTimeoutException e) {
		log.warn("Не удалось получить страницу " + url.toString());
		// все равно считаем, что ссылка рабочая
		crawlUrl(url);
		continue;
	    } catch (IOException e) {
		log.warn("Не удалось получить страницу " + url.toString());
		continue;
	    }

	    crawlUrl(url);

	    // найдено уже достаточно ссылок
	    if (urlsToCrawl.size() > maxLinkCount * 1.5)
		continue;

	    addLinks(doc);

	}

	stopwatch.stop();

	log.info("Найдено " + crawledUrls.size() + " ссылок . Затрачено времени : "
		+ stopwatch.elapsed(TimeUnit.SECONDS) + " секунд");

	callResult.put(firstUrl, crawledUrls);
	return callResult;
    }

    /**
     * Следущий адрес из {@link #urlsToCrawl}
     * 
     * @return
     */
    private String getNextUrl() {
	String url = urlsToCrawl.iterator().next();
	urlsToCrawl.remove(url);
	url = verifyUrl(url);
	return url;
    }

    /**
     * Находим все ссылки на страниц и добавляем в очередь {@link #urlsToCrawl}
     * 
     * @param doc
     */
    private void addLinks(Document doc) {

	Elements links = doc.select("a[href]");

	for (Element link : links) {

	    String fullHref = link.attr("abs:href");
	    if (isNullOrEmpty(fullHref))
		continue;

	    String newURL = verifyUrl(fullHref);

	    if (isNullOrEmpty(newURL))
		continue;

	    if (!crawledUrls.contains(newURL))
		urlsToCrawl.add(newURL);

	}

    }
}
