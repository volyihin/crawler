package org.crawler;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static java.util.concurrent.Executors.newFixedThreadPool;
import static org.crawler.utils.Urls.getUrlsList;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.crawler.utils.Urls;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Sets;

/**
 * Класс, запускает краулер с необходимыми настройками
 * 
 * @author volyihin@gmail.com
 * 
 */
public class CrawlerRunner {
    private static final Logger log = Logger.getLogger(CrawlerRunner.class);

    private int threadPool;
    private int pageTimeout;
    private int maxLinksCount;

    private long time = 0L;
    private long linkCount = 0L;

    public CrawlerRunner(int threadPool, int pageTimeout, int maxLinksCount) {
	super();
	this.threadPool = threadPool;
	this.pageTimeout = pageTimeout;
	this.maxLinksCount = maxLinksCount;
    }

    public void startCrawl() throws InterruptedException {

	Set<String> urls = getUrlsList();

	if (urls.size() == 0) {
	    log.info("Список доменов для исследования пуст");
	    return;
	}

	log.info("Добавлено " + urls.size() + " сайтов для исследования");

	// пул потоков
	ExecutorService executor = newFixedThreadPool(threadPool);
	// список фьючуров, в которых хранятся рузультаты потоков
	List<Future<Map<String, Set<String>>>> list = newArrayList();
	// список поток
	List<Callable<Map<String, Set<String>>>> callables = newArrayList();

	for (String url : urls) {
	    Callable<Map<String, Set<String>>> callable = new WebCrawler(url, pageTimeout, maxLinksCount);
	    callables.add(callable);
	}

	Stopwatch stopwatch = Stopwatch.createStarted();

	try {
	    log.info("Запускаем исследование ");

	    list = executor.invokeAll(callables);
	} catch (InterruptedException e1) {

	    throw new InterruptedException("Краулер был прерван");
	}

	executor.shutdown();

	Set<String> result = Sets.newHashSet();

	for (Future<Map<String, Set<String>>> fut : list) {
	    Map<String, Set<String>> currentSet = newHashMap();
	    try {
		currentSet = fut.get();

	    } catch (InterruptedException | ExecutionException e) {
		log.warn("Поток прерван.", e);
	    }
	    String key = currentSet.keySet().iterator().next();
	    log.debug("ВСЕГО " + key + "::" + currentSet.get(key).size());
	    result.addAll(currentSet.get(key));
	}
	time = stopwatch.elapsed(TimeUnit.SECONDS);
	linkCount = result.size();
	log.info("Закончили исследование " + time);
	log.info("Колличество ссылок = " + linkCount);

	Urls.writeResult(result);
    }

    public static void main(String[] args) throws IOException, InterruptedException {
	int threadPool = 64;
	int pageTimeout = 2000;
	int maxLinksCount = 100;

	for (int i = 0; i < 100; i++) {
	    CrawlerRunner runner = new CrawlerRunner(threadPool, pageTimeout, maxLinksCount);
	    runner.startCrawl();
	    System.err.println(runner.time + " секунд " + runner.linkCount + " ссылок");
	}
    }
}
